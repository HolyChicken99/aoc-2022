#!/usr/bin/env python3

file = open("./input.txt","r")
out = file.read()
out = out.split("\n")
len_arr = len(out) -1
total_count =0
for i in out :
    if i !="":
        s = i.split(",")
        s0=s[0]
        s1=s[1]
        s0=s0.split("-")
        s1=s1.split("-")
        s00=int(s0[0])
        s01=int(s0[1])
        s10=int(s1[0])
        s11=int(s1[1])
        if ( (s00 < s10) and (s01 < s10) ):
            total_count = total_count +1
        if ((s10 < s00) and (s11 < s00)):
            total_count = total_count+1


print(len_arr - total_count)
