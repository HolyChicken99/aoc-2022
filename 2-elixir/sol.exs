  {:ok, contents} = File.read("input.txt")
  arr = String.split(contents, "\n")

  # Rock defeats Scissors, Scissors defeats Paper, and Paper defeats Rock.
  map1 = %{"A" => 1, "B" => 2, "C" => 3}
  # Rock,paper,scissor
  map2 = %{"X" => 0, "Y" => 3, "Z" => 6}

  # X means you need to lose, Y means you need to end the round in a draw, and Z means you need to win.
  a =
    Enum.map(arr, fn x ->
      case x do
        "" ->
          0

        _ ->
          [player1, player2] = String.split(x, " ")
          # IO.puts(map2[player2])
          final_score = map2[player2]

          case [player1, player2] do
            ["A", "X"] ->
              final_score + 3

            ["C", "Z"] ->
              final_score + 1

            [val, "X"] ->
              final_score + map1[player1] - 1

            [val, "Y"] ->
              final_score + map1[player1]

            [val, "Z"] ->
              final_score + map1[player1] + 1
          end
      end
    end)

  # IO.inspect(a)
  IO.puts(Enum.sum(a))
