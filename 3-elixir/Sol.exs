{:ok, contents} = File.read("input.txt")

arr = String.split(contents, "\n") |> Enum.chunk_every(3)
IO.inspect(arr)

a =
  Enum.map(arr, fn x ->
    case x do
      [""] ->
        nil

      _ ->
        first = Enum.at(x, 0)
        second = Enum.at(x, 1)
        third = Enum.at(x, 2)
        IO.puts("the ting is #{first}")
        IO.puts("the ting is #{second}")
        IO.puts("the tinn is #{third}")
        map1 = String.graphemes(first) |> Enum.frequencies()
        map2 = String.graphemes(second) |> Enum.frequencies()
        map3 = String.graphemes(third) |> Enum.frequencies()

        Enum.map(map1, fn {k, _} ->
          if Map.has_key?(map2, k) do
            if Map.has_key?(map3, k) do
              k
            end
          end
        end)
    end
  end)

arr = List.flatten(a)
IO.inspect(arr)

arr = Enum.filter(arr, fn x -> x end)
IO.puts("the list is #{arr}")
arr = Enum.map(arr, fn x -> val = x |> String.to_charlist() |> hd end)

arr =
  Enum.map(arr, fn x ->
    cond do
      x > 96 ->
        x - 96

      true ->
        x - 38
    end
  end)

IO.inspect(Enum.sum(arr))
