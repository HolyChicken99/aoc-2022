// Hello World! program
using System.IO;
using System;
using System.Collections.Generic;
namespace HelloWorld
{
    class Hello {
        static void Main(string[] args)
        {
            string text = File.ReadAllText("./input.txt");
            string[] subs = text.Split("\n\n");
            string first_part = subs[0];
            string second_part=subs[1];
            int Length = first_part.Length;
            int line=0;
            foreach( char c in first_part ){
                if (c == '\n'){
                    line = line+1;
                }
            }
            line++;
            // Each stack is 4 character wide
            int Columns = ColumnNumber(ref first_part);
            List<List<string>> stacks = new List<List<string>>();

            for (int i = 0; i < Columns ; i++ ){
            stacks.Add(stack(ref first_part,i));
            }

            string[] instructions = second_part.Split('\n');
            foreach ( var s in instructions){
                if(s!=""){
                int number_of_elements = Int16.Parse(s.Substring(5,s.IndexOf('f')-6));
                int from_stack = Int16.Parse(s.Substring(12,s.IndexOf('t')-13));
                int to_stack = Int16.Parse(s.Substring(17,s.Length-17));
                List<string> rev = new List<string>();
                for(int i =0;i<number_of_elements;i++){
                    string top = stacks[from_stack-1][0];
                    stacks[from_stack-1].RemoveAt(0);
                    rev.Add(top);
                }
                rev.Reverse();
                foreach (var item in rev)
                {
                    stacks[to_stack-1].Insert(0,item);
                }

                }}
            foreach(var s in stacks){
                Console.WriteLine(s[0]);
            }
        }

        static int ColumnNumber(ref string first ){
            string[] subs= first.Split("\n");
            int max = 0;
            foreach(string s in subs){
                if (s.Length >= max){
                    max = s.Length;
                }
            }
            return ((max+1)/4);
        }

        static bool is_all_white(ref string first){
            foreach(var c in first){
                if(c!=' '){
                    return false;
                }
            }
            return true;
        }
        static List<string> stack(ref string first, int num){
            string[] subs = first.Split('\n');

            List<string> c = new List<string>();
            // Each stack is 4 character wide
            for (int i =0;i<subs.Length-1;i++){
                int start = num*4;
                int end = start + 4;
                var new_string=subs[i]+" ";
                // Console.WriteLine(end);
                // Console.WriteLine(subs[i].Length);
                if (!(end>(new_string.Length)))
                {
                    string substr= (new_string.Substring(start,4)).Substring(1,1);
                    if(!is_all_white(ref substr)){
                        // Console.WriteLine(substr);
                        c.Add(substr);
                    }
                }
            }
            return c;
        }
    }

}
