#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

int main() {
  int max = 0;
  int temp1 = 0;
  ifstream ifs;
  std::vector<int> pol;
  std::string s;
  ifs.open("./input.txt");
  while (getline(ifs, s)) {
    if (s != "") {
      int temp = 0;
      stringstream val(s);
      val >> temp;
      temp1 = temp1 + temp;
    } else if (s == "") {
      pol.push_back(temp1);
      temp1 = 0;
    }
  }
  std::sort(pol.begin(), pol.end(), std::greater<int>());

  std::cout << pol[0] + pol[1] + pol[2];

  return 0;
}
